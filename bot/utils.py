from telebot.types import (ReplyKeyboardMarkup, KeyboardButton,
                           InlineKeyboardMarkup, InlineKeyboardButton)
from bot import const
from users.models import Category


def get_main_menu(user):
    rkm = ReplyKeyboardMarkup(True, row_width=2)
    rkm.add(const.CARD[user.lang])
    rkm.add(*(category.title[user.lang] for category in Category.objects.all()))
    rkm.add(*(const.SETTINGS[user.lang],))
    return rkm


def get_inline_keyboard_category(product):
    ikbs = (
        InlineKeyboardButton('◀️', callback_data=f'back_{product.id}'),
        InlineKeyboardButton(f"{product.id}/{5}", callback_data='2'),
        InlineKeyboardButton("▶️", callback_data=f'forward_{product.id}')
    )
    ikb = InlineKeyboardButton('add to card', callback_data=f'add_{product.id}'),
    ikm = InlineKeyboardMarkup()
    ikm.add(*ikbs)
    ikm.add(*ikb)
    return ikm
