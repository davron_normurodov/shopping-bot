
ASK_LANGUAGE = "🇺🇿 Tilni tanlang\n🇷🇺 Выберите язык"

LANG_CHOOSE_MENU = {
    "uz": "Uzbek 🇺🇿",
    "ru": "Русский 🇷🇺"
}

ASK_CONTACT_NUMBER = {
    "uz": "Telefon raqamingizni kiriting",
    "ru": "Введите свой номер телефона"
}

SEND_CONTACT = {
    "uz": "Raqam yuborish",
    "ru": "Отправить номер"
}

# mainmenu
CATEGORIES = [
    {
        "uz": "Badiiy",
        "ru": "Художественная"
    },
    {
        "uz": "Diniy",
        "ru": "Pелигиозный"
    },
    {
        "uz": "Darsliklar",
        "ru": "Учебники"
    },
    {
        "uz": "Dasturlash",
        "ru": "Программирование"
    }
]
SETTINGS = {
    "uz": "Sozlamalar",
    "ru": "Настройки"
}
CARD = {
    "uz": "📥 Savat",
    "ru": "📥 Корзина"
}

CARD_INFO = {
    "uz": "Savatingiz hozircha bo'sh",
    "ru": "Ваша корзина сейчас пуста"
}

TOTAL_PRICE = {
    "uz": "Umumiy summa",
    "ru": "Общая сумма"
}
