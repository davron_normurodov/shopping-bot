from users.models import User, Order, OrderProduct
from bot import const


def get_user(chat_id):
    user = User.objects.filter(chat_id=chat_id).first()
    if not user:
        return None
    return user


def set_lang(user, lang):
    user.lang = lang
    user.save()


def set_contact(user, phone_number):
    user.contact_number = phone_number
    user.save()


def get_state(chat_id):
    return get_user(chat_id).state if get_user(chat_id) else None


def set_state(chat_id, state):
    user = get_user(chat_id)
    user.state = state if state else ''
    user.save()


def add_to_card(user, chat_id, product_id):
    order = Order.objects.filter(user__chat_id=chat_id, status='active').first()
    if order:
        order_product = OrderProduct.objects.filter(order_id=order.id, product_id=product_id).first()
        if not order_product:
            order_product = OrderProduct.objects.create(order_id=order.id, product_id=product_id, amount=1)
        else:
            order_product.amount += 1
            order_product.save()
    else:
        order = Order.objects.create(user_id=user.id)
        order_product = OrderProduct.objects.create(order_id=order.id, product_id=product_id, amount=1)
    order.total_price += order_product.product.price
    order.save()


def show_card(user):
    order = Order.objects.filter(user_id=user.id, status='active').first()
    msg = ''
    if order:
        for op in order.products.all():
            msg += f'{op.product.title[user.lang]}:\n\t\t{op.product.price} \
* {op.amount} = {op.product.price * op.amount}\n'
        msg += f'{const.TOTAL_PRICE[user.lang]} = {order.total_price}'
    else:
        msg += const.CARD_INFO[user.lang]
    return msg


def clear_card(user):
    pass
