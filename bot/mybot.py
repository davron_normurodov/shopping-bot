import telebot
from telebot.types import (ReplyKeyboardMarkup, KeyboardButton,
                           ReplyKeyboardRemove, InlineKeyboardMarkup, InlineKeyboardButton)
from users.models import User, Product, Order, OrderProduct
from bot import const, db_utils, utils

Token = '5119668957:AAHdudB7iRg56D-HfT8FAzn-bHRyNxdziwQ'


class UserState:
    start = 0
    language = 1
    contact = 2
    main_menu = 3
    choose_product = 4
    show_card = 5


bot = telebot.TeleBot(token=Token)


@bot.message_handler(commands=['del'])
def command_help(message):
    chat_id = message.from_user.id
    User.objects.get(chat_id=chat_id).delete()
    bot.send_message(chat_id, 'Deleted', reply_markup=ReplyKeyboardRemove())


@bot.message_handler(commands=['start'])
def command_help(message):
    chat_id = message.from_user.id
    user_info(chat_id)


@bot.message_handler(func=lambda message: db_utils.get_state(message.from_user.id)==UserState.language)
def choose_language(message):
    text = message.text
    chat_id = message.from_user.id
    user = db_utils.get_user(chat_id)
    if not user:
        pass
        # error
    elif text == const.LANG_CHOOSE_MENU['uz']:
        db_utils.set_lang(user, 'uz')
        ask_contact(chat_id, user.lang)
    elif text == const.LANG_CHOOSE_MENU['ru']:
        db_utils.set_lang(user, 'ru')
        ask_contact(chat_id, user.lang)
    else:
        pass
        # error


@bot.message_handler(content_types=['contact'])
def contact_handler(message):
    phone_number = message.contact.phone_number
    chat_id = message.from_user.id
    user = db_utils.get_user(chat_id)
    db_utils.set_contact(user, phone_number)
    go_main_menu(user)


@bot.message_handler(func=lambda message: db_utils.get_state(message.from_user.id)==UserState.main_menu)
def main_menu_handler(message):
    text = message.text
    chat_id = message.from_user.id
    user = db_utils.get_user(chat_id)
    if not user:
        #error
        pass
    elif text == const.CATEGORIES[0][user.lang]:
        show_product_list(user, const.CATEGORIES[0])
    elif text == const.CATEGORIES[1][user.lang]:
        show_product_list(user, const.CATEGORIES[1])
    elif text == const.CATEGORIES[2][user.lang]:
        show_product_list(user, const.CATEGORIES[2])
    elif text == const.CARD[user.lang]:
        msg = db_utils.show_card(user)
        bot.send_message(chat_id, msg)
        db_utils.set_state(chat_id, UserState.show_card)


@bot.message_handler(func=lambda message: db_utils.get_state(message.from_user.id)==UserState.show_card)
def card_handler(message):
    text = message.text
    chat_id = message.from_user.id
    user = db_utils.get_user(chat_id)
    if not user:
        #error
        pass


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    chat_id = call.from_user.id
    data = call.data
    user = db_utils.get_user(chat_id)
    step = data.split('_')[0]
    product_id = data.split('_')[1]
    message_id = call.message.message_id
    if step == 'add':
        db_utils.add_to_card(user, chat_id, product_id)
    elif step in ('forward', 'back'):
        show_next_product(user, product_id, message_id, step)


"""============================ Activities ==============================="""


def user_info(chat_id):
    user = db_utils.get_user(chat_id)
    if not user:
        user = User.objects.create(chat_id=chat_id)
        ask_language(chat_id)
    elif not user.lang:
        ask_language(chat_id)
    elif not user.contact_number:
        ask_contact(chat_id, user.lang)


def ask_language(chat_id):
    rkm = ReplyKeyboardMarkup(True, row_width=2)
    rkm.add(const.LANG_CHOOSE_MENU['uz'], const.LANG_CHOOSE_MENU['ru'])
    bot.send_message(chat_id, const.ASK_LANGUAGE, reply_markup=rkm, parse_mode='HTML')
    # bot.set_state(user_id=chat_id, state=UserState.language)
    db_utils.set_state(chat_id, UserState.language)


def ask_contact(chat_id, lang):
    rkm = ReplyKeyboardMarkup(True).add(KeyboardButton(const.SEND_CONTACT[lang], request_contact=True))
    bot.send_message(chat_id, const.ASK_CONTACT_NUMBER[lang], reply_markup=rkm, parse_mode='HTML')
    db_utils.set_state(chat_id, UserState.contact)


def go_main_menu(user):
    bot.send_message(user.chat_id, 'main menu', reply_markup=utils.get_main_menu(user))
    db_utils.set_state(user.chat_id, UserState.main_menu)


def show_product_list(user, category):
    product = Product.objects.filter(category__title=category).order_by('id').first()
    caption = f'{product.title[user.lang]}\n\n{product.price}'
    bot.send_photo(user.chat_id, product.image, caption, reply_markup=utils.get_inline_keyboard_category(product))


def show_next_product(user, product_id, message_id, step):
    chat_id = user.chat_id
    product_cat = Product.objects.get(id=product_id).category
    products = Product.objects.filter(category=product_cat).order_by('id')

    next_product = products.filter(id__gt=product_id).first() if step == 'forward' \
        else products.filter(id__lt=product_id).last()
    if next_product:
        caption = f'{next_product.title[user.lang]}\n\n{next_product.price}'
        bot.delete_message(chat_id, message_id)
        bot.send_photo(chat_id, next_product.image, caption,
                       reply_markup=utils.get_inline_keyboard_category(next_product))
    else:
        next_product = products.first() if step == 'forward' else products.last()
        caption = f'{next_product.title[user.lang]}\n\n{next_product.price}'
        bot.delete_message(chat_id, message_id)
        bot.send_photo(chat_id, next_product.image, caption,
                       reply_markup=utils.get_inline_keyboard_category(next_product))
